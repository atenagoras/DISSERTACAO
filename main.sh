#!/bin/bash

# Limpar pasta output
cd $(dirname $0)
rm -r output >& /dev/null
mkdir output
mkdir output/radioBRAMS # Saída da comparação do BRAMS com a radiossondagem
for i in pressao temperatura UR Vento tabelas
do
  mkdir output/radioBRAMS/$i
done
for i in Vento_U Vento_V DIR magnitude
do
  mkdir output/radioBRAMS/Vento/$i
done

mkdir output/SIMvsOBS # Saída da comparação do BRAMS/SPRAY com os dados na estação
for i in figuras tabelas
do
  mkdir output/SIMvsOBS/$i
  for j in CO meteo MP10
  do
    mkdir output/SIMvsOBS/$i/$j
  done
done
for i in Pressao Temperatura Umidade Vento
do
  mkdir output/SIMvsOBS/figuras/meteo/$i
done

mkdir output/SeriesHistoricas # Saída de gráficos de séries temporais de poluentes medidos e/ou estimativas de emissões

# Rodar scripts R
cd scripts/R
  ./roda.sh
  ./geratabelas.sh
  ./rodaCompModel.sh
  Rscript Rcsv2latexfull.R
  Rscript SerieHistoricaCO.R                # Série Histórica para CO em Congonhas
  Rscript CCesarSerieHistoricaCO.R          # Série Histórica para CO em Cerqueira César
  Rscript SerieHistoricaMP10.R              # Série Histórica para MP10 em Cubatão - Vila Parisi
  Rscript SerieHistoricaMP10CubataoCentro.R # Série Histórica para MP10 em Cubatão - Centro
  Rscript IbirapueraO3SerieHistorica.R      # Série Histórica para O3 no Ibirapuera
  Rscript MoocaO3SerieHistorica.R           # Série Histórica para O3 na Mooca
cd ../..

# Rodar ncl

# Rodar os shell Scripts

# Rodar latex
cd latex
  make
cd ..
