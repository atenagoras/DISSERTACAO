# Dissertação de Mestrado do IFUSP: Investigação sobre o transporte de poluentes em Cubatão, Região Metropolitana de São Paulo e arredores

# Conteúdo e razão de ser dos diretórios
latex   --> Dissertação
data    --> Dados a serem processados pelos scripts, ou dados fixos
scripts --> Scripts em várias linguagens para processar dados

## Instruções para trabalhar com múltiplas versões (branches - ramos)
1) Colando a branche master (principal) para uma nova branche (ramo):

    git checkout -b vANO-MÊS-DIA
    
2) Voltando para a branch master

    git checkout master

Hey Ate, add more instructions!
