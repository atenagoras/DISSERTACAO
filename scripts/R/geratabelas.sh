#!/bin/bash
for i in CO O3 PM10 PM25 NO2 SO2
do
    ln -sf ../../data/PQar/${i}tconfig.R ../../data/PQar/tableConfig.R
    Rscript Rcsv2latex.R
done
