#!/bin/bash
for i in P UR # looping para rodar a comparação  entre Radiossondagem e simulação para as variáveis Pressão, Umidade Relativa
do
   ln -sf confradiobrams/${i}configRadioBRAMS.R config.R
  Rscript comparaRadioBRAMS.R
done

Rscript VentocomparaRadioBRAMS.R # Comparação de Vento entre Radiossondagem e Simulação BRAMS
Rscript TcomparaRadioBRAMS.R # Comparação de Temperatura entre Radiossondagem e Simulação BRAMS
