#/bin/bash
for i in Pressao Umidade Temperatura CO MP10 MP10Full; do
    echo $i
    ln -sf confCompModel/${i}CompModelconfig.R CompModelconfig.R
    echo "Gráfico"
    Rscript CompModel.R
    echo "Estatística"
    Rscript StatCompModel.R
done
ln -sf confCompModel/VentoCompModelconfig.R VentoCompModelconfig.R
echo "Gráfico"
Rscript VentoCompModel.R
echo "Estatística"
Rscript StatVentoCompModel.R
