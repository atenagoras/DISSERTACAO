# Arquivo de Configuração para Comparação de Modelos com Dados Medidos
# Útil para os programas de gráficos de:
# - Séries Temporais
# - Espalhamento de Dados
# - Tabelas Estatísticas

# Parte comum: Leitura de arquivos .csv com os dados

din  <- "../../data/DadosEstacoesComp/meteo/" # Diretório de trabalho onde estão os .csv para
                                        # cada variável simulada/observada
dout <- "../../output/SIMvsOBS/figuras/meteo/" # Diretório de trabalho para jogar as figuras.
                                         # Acrescentar meteo/ na configuração das variáveis meteorológicas
Varq  <- c("TempCuCentroStats.csv",
           "TempIAG_Stats.csv",
           "TempIbirapueraStats.csv",
           "TempPinheirosStats.csv",
           "TempSaoCaetanoStats.csv",
           "TempSaoJoseCamposStats.csv",
           "TempTaboaoStats.csv") # Vetor com os nomes dos arquivos
lskip <- 10 # número de linhas para pular ao ler os arquivos .csv
csim  <- 7  # Coluna dos valores simulados
cobs  <- 10 # Coluna dos valores medidos
estacoes <- c("CubataoCentro",
              "IAG",
              "Ibirapuera",
              "Pinheiros",
              "SaoCaetano",
              "SaoJoseCampos",
              "Taboao") # Vetor para formar nomes de arquivos de saída (arquivos gráficos)
Estacoes <- c("Cutabão Centro",
              "IAG",
              "Ibirapuera",
              "Pinheiros",
              "São Caetano",
              "S José dos Campos",
              "Taboão da Serra") # Vetor com os nomes das estações
variavel  <- "Temperatura" # Variáveis disponíveis (exatamente como escritas aqui):
                  # Pressao; Temperatura Umidade; Vento; CO; MP10
labY      <- "Temperatura (ºC)"
# variáveis e parâmetros para tabelas de estatística (StatCompModel.R)
dtout <- "../../output/SIMvsOBS/tabelas/meteo/" # Diretório de saída de tabelas. O script acrescenta o nome da variável
Colmediaerro   <- c("Estação", "$\\overline{T}_{sim}$", "$\\overline{T}_{obs}$",
                  "$\\sigma_{sim}$","$\\sigma_{obs}$", "RMSE", "NRMSE","$\\frac{SIM}{OBS}$") # Nomes das colunas na tabela de médias e erros
Umediaerro     <- c("","ºC","ºC",
                    "ºC","ºC","ºC","\\%","") # Unidades dos parâmetros na tabela mediaerro.
Colperformance <- c("Estação", "n", "FB", "NMSE", "FAC2", "R", "valor-P") # Nomes das colunas na tabela de parâmetros de
                                                                          # performance da simulação
