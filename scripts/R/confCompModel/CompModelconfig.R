# Arquivo de Configuração para Comparação de Modelos com Dados Medidos
# Útil para os programas de gráficos de:
# - Séries Temporais
# - Espalhamento de Dados
# - Tabelas Estatísticas

# Parte comum: Leitura de arquivos .csv com os dados

din  <- "../../data/DadosEstacoesComp/" # Diretório de trabalho onde estão os .csv para
                                        # cada variável simulada/observada
dout <- "../../output/SIMvsOBS/figuras/" # Diretório de trabalho para jogar as figuras.
                                         # Acrescentar meteo/ na configuração das variáveis meteorológicas
Varq  <- c("CO_CampinasCentro.csv",
           "CO_Ccesar.csv",
           "CO_Centro.csv",
           "CO_Congonhas.csv",
           "CO_Osasco.csv",
           "CO_Ibirapuera.csv",
           "CO_Jundiai.csv",
           "CO_PedroII.csv",
           "CO_Pinheiros.csv",
           "CO_SantoAmaro.csv",
           "CO_SantoAndrecsv.csv", 
           "CO_SaoCaetano.csv",
           "CO_TaboaoCongonhasMod2006.csv") # Vetor com os nomes dos arquivos
lskip <- 10 # número de linhas para pular ao ler os arquivos .csv
csim  <- 7  # Coluna dos valores simulados
cobs  <- 10 # Coluna dos valores medidos
estacoes <- c("CampinasCentro",
              "Ccesar",
              "Centro",
              "Congonhas",
              "Osasco",
              "Ibirapuera",
              "Jundiai",
              "PedroII",
              "Pinheiros",
              "SantoAmaro",
              "SantoAndre", 
              "SaoCaetano",
              "Taboao") # Vetor para formar nomes de arquivos de saída (arquivos gráficos)
Estacoes <- c("Campinas Centro",
              "Cerqueira César",
              "Centro",
              "Congonhas",
              "Osasco",
              "Ibirapuera",
              "Jundiaí",
              "Pedro II",
              "Pinheiros",
              "Santo Amaro",
              "Santo André", 
              "São Caetano",
              "Taboão da Serra") # Vetor com os nomes das estações
variavel  <- "CO" # Variáveis disponíveis (exatamente como escritas aqui):
                  # Pressao; Temperatura Umidade; Vento; CO; MP10
labY      <- as.expression("CO ("~mu~"g"~m^-3~")") # É necessário usar as.expression, ou expression para usar 
                                                   # superscript (^), subscript ([objeto em subscript]) e 
                                                   # letras gregas e ~ para emendar com o resto das palavras.
# variáveis e parâmetros para tabelas de estatística (StatCompModel.R)
dtout <- "../../output/SIMvsOBS/tabelas/" # Diretório de saída de tabelas. O script acrescenta o nome da variável
Colmediaerro   <- c("Estação", "$\\overline{CO}_{sim}$", "$\\overline{CO}_{obs}$",
                  "$\\sigma_{sim}$","$\\sigma_{obs}$", "RMSE", "NRMSE","$\\frac{SIM}{OBS}$") # Nomes das colunas na tabela de médias e erros
Umediaerro     <- c("","$\\mu g m^{-3}$","$\\mu g m^{-3}$",
                    "$\\mu g m^{-3}$","$\\mu g m^{-3}$","$\\mu g m^{-3}$","\\%") # Unidades dos parâmetros na tabela mediaerro.
Colperformance <- c("Estação", "n", "FB", "NMSE", "FAC2", "R", "valor-P") # Nomes das colunas na tabela de parâmetros de
                                                                          # performance da simulação
                                                                        

#unidade  <- expression(~mu~"g"~m^-3) # Para imprimir unidade na etiqueta do eixo X, junto com a variável
