# Este programa faz a interpolação de variáveis da simulação
# para o nível das radiossondagens, compara-as, faz gráficos e estatística
# inicialmente, trabalhando com exemplos
# 
#args = commandArgs( trailingOnly = TRUE) # lê os argumentos a partir da 
rm(list=ls())
#library('Hmisc') # Bibliteca para plotar gráficos (função plot)
library('xtable') # para gerar tabelas em LateX
source('functions.R')
source('config.R')

# Início do código principal

# Declaração de variáveis iniciais ...
# Para leitura de arquivos de simulação
simpath = "../../data/meteocujuspGradeMaior/"
# simfile = "P_BRAMSzlevels.csv" # Lido a partir de config.R
SIMFILE <- paste(simpath,simfile,sep="")
SIMDATA <- LeSIM(SIMFILE) #Armazena as variáveis da simulação a esta matriz

# Para leitura de arquivos de radiossondagem
soundingpath = "../../data/Radiossondagem/"
agosto2006 = "-08-2006-"
csv = ".csv"
horas = c("00h","12h")
corpo ="CampoDeMarte"
dia = c(23:29)

# armazenamento de resultados
dirfiguras = sprintf("../../output/radioBRAMS/%s/",vari)
dirtabelas = "../../output/radioBRAMS/tabelas/"
# strings para preencher gráfico
# titulo = "Radiossondagem/Resultados BRAMS (interpolação)" # Lido a partir de config.R
#captionX = "Altura (nível do mar)" # Lido a partir de config.R
#captionY = "Pressão" # Lido a partir de config.R

# Ação
DATA        <- NULL # Variáveis para escrever tabela com correlação
CORR        <- NULL
all_sim_y   <- NULL # todos valores interpolados da simulação nos níveis da Radiossondagem em todos os dias
all_radio_y <- NULL # todos valores da Radiossondagem em todos os dias
P           <- numeric(length(dia)*length(horas)) # nível de significância p para todas as radiossondagens
p           <- 1 # Índice para o nível de significância de cada radiossondagem
N           <- numeric(length(dia)*length(horas)) # Número de pontos comparados. Tem de ser igual ao número de pontos interpolados em cada Radiossondagem

# Padronização da escala X dos gráficos
## Lendo todas as radiossondagens
for (i in dia){
  for (j in horas){
    soundingfile <- paste(soundingpath,corpo, i, agosto2006, j, csv, sep="") # Nome do arquivo de Radiossondagem a abrir
    print(soundingfile)
    soundingdata <- LeSounding(soundingfile) # Dados da Radiossondagem. Coluna 1 é a altura. Coluna 2 é a pressão
    all_radio_y <- c(all_radio_y, as.numeric(soundingdata[,Nvar]))
  }
}
Range <- OverallxRange(SIMDATA, all_radio_y)
# Resetandoo a variável para ser utilizada no Looping
all_radio_y <- NULL

k=2 # para contar as colunas de pressão para cada dia e horário
# (00h e 12h, sequencialmente) dentro do próximo looping
for (i in dia){
  for (j in horas){
    soundingfile <- paste(soundingpath,corpo, i, agosto2006, j, csv, sep="") # Nome do arquivo de Radiossondagem a abrir
    print(soundingfile)
    soundingdata <- LeSounding(soundingfile) # Dados da Radiossondagem. Coluna 1 é a altura. Coluna 2 é a pressão
    interpolacaobrams <- approx(SIMDATA[,1], SIMDATA[,k], soundingdata[,1]) # Interpola os valores as pressões do BRAMS
                                                                            # nos valores de altura da Radiossondagem
                                                                            # para posterior comparação
    # Código para plotagem
    subtitulo = sprintf("%s %d/08/2006 %s (UTC)", strVAR,i , j)
    imgfile = sprintf("%simg%s%d-08-2006-%s.pdf", dirfiguras, vari, i, j)
    Plota(soundingdata, interpolacaobrams, captionX, captionY, titulo, subtitulo, imgfile, Nvar, Range)
    # Inserir código para a correlação
    corr   <- cor(as.numeric(soundingdata[,Nvar]), as.numeric(interpolacaobrams$y), method = "pearson", use = "complete.obs")
    CORR   <- c(CORR, corr)
    testeP <- cor.test(as.numeric(soundingdata[,Nvar]), as.numeric(interpolacaobrams$y), use = "complete.obs" )
    P[p]   <- testeP$p.value
    N[p]   <- length(na.omit(interpolacaobrams$y)) # na.omit exclui os NAs do resultado que vai para length
    Data   <- sprintf("%d/08/2006 %s", i, j)
    DATA   <- c(DATA, Data)
    all_sim_y   <- c(all_sim_y, interpolacaobrams$y)
    all_radio_y <- c(all_radio_y, soundingdata[,Nvar])
    k = k+1 # alterando a coluna para leitura da pressão sequencialmente
    p = p+1 # alterando o índice para armazenar nível de significância da próxima radiossondagem
  }
}
# Código para escrever tabela (fazer função para isso em outro arquivo)
arqtable <- sprintf("%scorr_%s.tex", dirtabelas, vari)
TabelaCorrelacaoLateX(CORR, DATA, N, P, all_sim_y, all_radio_y, tcaption, tlabel, arqtable)
