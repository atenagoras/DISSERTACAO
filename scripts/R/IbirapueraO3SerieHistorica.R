Planilha <- read.csv("../../data/IbirapueraO3PQAr.csv", header = FALSE, skip = 2, sep = ";", dec = ",")
Ano                <- Planilha[,1]
UltrapassagensPQAr <- Planilha[,2]
Atencao            <- Planilha[,3]
max1               <- Planilha[,4]
max2               <- Planilha[,5]
# Padrão, Atenção e Alerta
PQAr      <- 160 # CONAMA
atencaoBR <- 400 # CONAMA
AlertaBR  <- 800 # CONAMA
atencaoSP <- 200
AlertaSP  <- 400

legenda = sprintf("Número de Ultrapassagens de padrões para O3\nEstação Ibirapuera")
pdf(file = "../../output/SeriesHistoricas/UltrapassagensO3Ibirapuera.pdf") # Gráfico de Número de Ultrapassagens
plot(UltrapassagensPQAr~Ano,
     xlab = "Ano", ylab = "Número de ultrapassagens",
     ylim = c(0,75),
     type = "l",col = "blue",
     main = legenda)
points(UltrapassagensPQAr~Ano, pch = 3, col = "blue")
lines(Atencao~Ano, type = "l", col = "red")
points(Ano, Atencao, pch = 18, col = "red")
legend("topright", c("PQAr (horas)","Atenção (dias)"), col = c("blue", "red"), pch = c(3, 18))
dev.off()

pdf(file = "../../output/SeriesHistoricas/ConcentracoesO3Ibirapuera.pdf") # Gráfico com concentrações
legenda = sprintf("Série Histórica de Concentração de O3 em Ibirapuera\nMédia 1h")
plot(max1~Ano,
     xlab = "Ano", ylab = as.expression(O[3]~" ("~mu~"g"~m^-3~")"), type = "l", col = "violet",
     ylim = c(0,550),
     main = legenda)
lines(c(1980,2015), c(PQAr,PQAr), type = "l", col = "green", lty = 1)
lines(c(1980,2015),c(atencaoSP,atencaoSP), type = "l", col = "orange", lty = 1)
lines(c(1980,2015),c(AlertaSP,AlertaSP), type = "l", col = "red", lty = 1)
lines(c(1980,2015),c(atencaoBR,atencaoBR), type = "l", col = "black", lty = 3)
points(max1~Ano, pch = 3, col = "violet")
lines(max2~Ano, type = "l", col = "blue")
points(max2~Ano, pch = 5, col = "blue")
legend("topright", c("PQAr", "Atenção SP", "Alerta SP", "Atenção BR", "1ª Máxima", "2ª Máxima"),
       col = c("green","orange","red","black","violet","blue"),
       lty = c(1,1,1,3), pch = c(NA,NA,NA,NA,3,5))
dev.off()