;Converte pontos em Lat/Lon para UTM
; Posição das indústrias de Cubatão (ano 2006)

; Pontos em lat/lon
; Indústrias de Fertilizantes e insumos agrícolas
llBungeFert1       = (/-23.846861,-46.379659/); Bunge Fertilizantes - Planta 1 (antiga IAP)
llBungeFert2       = (/-23.841721,-46.374804/); Bunge Fertilizantes - Planta 2 (antiga Manah)
;llBungeFert1       = (/-23.856619,-46.386317/); Coordenadas do Google
;llIAP_rf           = (/-23.846248,-46.379904/); Antiga planta 2 da IAP
;llManah_sc         = (/-23.840819,-46.375000/); Antiga planta 2 da Manah
llCopebras         = (/-23.840774,-46.391596/); Localização Antiga
;llCopebras         = (/-23.857308,-46.362269/); Coordenadas Google
llUltrafertil_pi   = (/-23.837392,-46.378286/); Complexo Piaçaguera
;llUltrafertil_pi  = (/-23.874612,-46.434764/); Complexo Piaçaguera - Coordenadas Google
llUltrafertil_cu   = (/-23.877836,-46.426031/); Complexo Cubatão
llUltrafertil_mar  = (/-23.874612,-46.434764/); Complexo Terminal Marítimo
llMosaic          = (/-23.877871,-46.426122/); Mosaic Fertilizantes
llIFC             = (/-23.857308,-46.390014/); Indústria de Fertilizantes Cubatão - IFC

; Indústrias não fertilizantes (complexo petroquímico, siderurgia cimento, papel etc)
llVotorantim      = (/-23.848067,-46.390817/); Votorantim Cimentos
llCarboCloro      = (/-23.877851,-46.426175/); CarboCloro Indústrias Químicas
llColumbian       = (/-23.843111,-46.392016/); Columbian Chemicals Brasil Ltda
llCBE             = (/-23.878243,-46.424244/); Companhia Brasileira de Estireno - CBE
llCOSIPA          = (/-23.861741,-46.373751/); Companhia Siderúrgica Paulista - COSIPA
llDow             = (/-23.877453,-46.426646/); Dow Chemical Brasil (majoritariamente, indústria de plásticos, tintas etc)
llPetrobrasRPB    = (/-23.871611,-46.430051/); Refinaria Presidente Bernardes da Petrobras
llPetrocoque      = (/-23.866923,-46.420001/); Petrocoque
llRipasa          = (/-23.885795,-46.452223/); Ripasa (atual MD Papéis)

; Pontos em utm
; Fábricas de Fertilizantes e insumos agrícolas
utmBungeFert1      = latlon2utm(llBungeFert1,2)
utmBungeFert2      = latlon2utm(llBungeFert2,2)
utmCopebras        = latlon2utm(llCopebras,2)
utmUltrafertil_pi  = latlon2utm(llUltrafertil_pi,2)
utmUltrafertil_cu  = latlon2utm(llUltrafertil_cu,2)
utmUltrafertil_mar = latlon2utm(llUltrafertil_mar,2)
utmMosaic          = latlon2utm(llMosaic,2)
utmIFC             = latlon2utm(llIFC,2)
; Fábricas dos demais complexos industriais
utmVotorantim      = latlon2utm(llVotorantim,2)
utmCarboCloro      = latlon2utm(llCarboCloro,2)
utmColumbian       = latlon2utm(llColumbian,2)
utmCBE             = latlon2utm(llCBE,2)
utmCOSIPA          = latlon2utm(llCOSIPA,2)
utmDow             = latlon2utm(llDow,2)
utmPetrobrasRPB    = latlon2utm(llPetrobrasRPB,2)
utmPetrocoque      = latlon2utm(llPetrocoque,2)
utmRipasa          =latlon2utm(llRipasa,2)
; Imprimindo pontos das fábricas em UTM
print("______________________________________________")
print("Fábricas de Fertilizantes e insumos agrícolas")
print("______________________________________________")
print("Bunge Fertilizantes - Planta 1")
print("---------------------------------")
print(utmBungeFert1)
print("=================================")
print("Bunge Fertilizantes - Planta 2")
print("---------------------------------")
print(utmBungeFert2)
print("=================================")
print("Copebras")
print("---------------------------------")
print(utmCopebras)
print("=================================")
print("Ultrafértil - Complexo Piaçaguera")
print("---------------------------------")
print(utmUltrafertil_pi)
print("=================================")
print("ULTRAFERTIL - Complexo Cubatão")
print("---------------------------------")
print(utmUltrafertil_cu)
print("=========================================")
print("ULTRAFERTIL - Complexo Terminal Martítimo")
print("-----------------------------------------")
print(utmUltrafertil_mar)
print("=================================")
print("Mosaic Fertilizantes")
print("---------------------------------")
print(utmMosaic)
print("========================================")
print("Indústria de Fertilizantes Cubatão - IFC")
print("----------------------------------------")
print(utmIFC)
print("______________________________________________")
print("Outros complexos industriais")
print("______________________________________________")
print("Votorantim Cimentos")
print("----------------------------------------")
print(utmVotorantim)
print("========================================")
print("CarboCloro Indústrias Químicas")
print("----------------------------------------")
print(utmCarboCloro)
print("========================================")
print("Columbian Chemicals Brasil Ltda")
print("----------------------------------------")
print(utmColumbian)
print("========================================")
print("Companhia Brasileira do Estireno - CBE")
print("----------------------------------------")
print(utmCBE)
print("========================================")
print("COSIPA")
print("----------------------------------------")
print(utmCOSIPA)
print("========================================")
print("Down Brasil")
print("----------------------------------------")
print(utmDow)
print("==========================================")
print("Petrobras - Refinaria Presidente Bernardes")
print("------------------------------------------")
print(utmPetrobrasRPB)
print("========================================")
print("Petrocoque")
print("----------------------------------------")
print(utmPetrocoque)
print("========================================")
print("Ripasa")
print("----------------------------------------")
print(utmRipasa)