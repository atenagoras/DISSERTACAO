# Sexta-feira 22/01/2016
# Extração do perfil vertical das variáveis meteorológicas do arquivo
# de meteorologia
# A sintaxe é:
# postbin-2.4.3 <arquivo.bin> <arquivo de parâmetros.ini> <arquivo de pontos> <data inicial> <data final> <frequência>
# Se for colocado -1 no lugar da data inicial, pega-se a primeira existente no arquivo,
# Se for no lugar data final, pega-se a última existente,
# Se for no lugar da frequência, pegam-se todas os intervalos para as variáveis
# Atenágoras Souza Silva
postbin-2.4.3 ../CUjuspG3meteoAnal.utc.nc relevoCmarte.ini CampoDeMarte.coo relevoCMarte_ -1 -1 6
