tlabel   = "table: pqMP25"
tcaption = "Padrões de Qualidade do Ar para $MP_{2,5}$:\\ Valores estabelecidos pelo CONAMA e OMS.\\ As metas referem-se a padrões intermediários sugeridos pela OMS antes da principal ser atingida."
arq      = "PM25-CONAMA-WHO"