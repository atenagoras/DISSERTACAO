\section{Modelos Estocásticos Lagrangianos - Uma Introdução}
Mais de um século antes da era atômica e da corrida armamentista, já havia preocupação em regular a poluição do ar em suas fontes, entretanto, tais tentativas fracassaram.
Com o advento da capacidade de quebrar átomos instáveis para gerar energia, tanto para propósitos bélicos quanto pacíficos, a necessidade de prever os efeitos das emissões de poluentes tornou-se muito mais premente, dado o potencial de danos de acidentes nucleares, fazendo com que meteorologistas estudassem com mais afinco a dispersão de poluentes atmosféricos e os governos das superpotências fossem mais criteriosos no controle das atividades atômicas, em relação à regulação de outras.

O desenvolvimento contido neste capítulo foi baseado especialmente em \citet{csanady2012turbulent}, pois oferece um desenvolvimento tão pedestre quanto completo da teoria de difusão atmosférica, seja na formulação euleriana, ou lagrangeana e estocástica para uma atmosfera homogênea e estacionária.

Para o desenvolvimento das equações do SPRAY e como ele lida com uma atmosfera mais próxima do real (turbulência nem homogênea, nem estacionária), o texto acompanha \citet{ManualSPRAY}.

\subsection{Difusão Atmosférica}
A difusão na atmosfera possui três causas: simples advecção por conta do vento médio, difusão turbulenta por conta do vento turbulento e difusão molecular. A difusão molecular na troposfera é desprezível comparada às outras.

Entretanto, para entender como a difusão turbulenta funciona, é necessário estudar a difusão molecular, sob a qual sua teoria foi desenvolvida por analogia.
\subsection{Difusão Molecular - Formulação Euleriana}
Seja $\chi$ a concentração de um poluente na atmosfera, em unidades de massa por volume, num determinado volume fechado.%

\begin{equation}
  \label{eq:Conc}
  \chi = \frac{\Delta M}{\Delta V}
\end{equation}
Onde M é a massa e V, o Volume. $V \gg a³$, onde a é a distância entre as partículas na difusão.

Seguindo a lei de Fick, o fluxo, dado em unidades de massa por tempo e área, considerando apenas a difusão neste volume é:
\begin{equation}
  \label{eq:Fluxo}
  \overrightarrow{F} = -D\overrightarrow{\nabla}\chi
\end{equation}
Onde D é a constante de difusão.
\subsubsection{Conservação de Massa}
A massa total no volume, em um dado instante é:
\begin{equation}
  \label{eq:Mtotal}
  M(t) = \oint_V \chi dV
\end{equation}

O fluxo total no volume, em um dado instante, é:
\begin{equation}
  \label{eq:Flxtotal}
  J(t) = \oint_S\overrightarrow{F}.\widehat{n}dS
\end{equation}
Onde $\widehat{n}$ é o vetor normal à superfície.
Se não há fontes ou sumidouros, a massa se conserva, então:
\begin{equation}
  \label{eq:Flxtotal2}
  J = -\frac{\partial M}{\partial t}
\end{equation}
Usando o teorema da divergência \ref{eq:Tdivergencia}, podemos obter a equação da continuidade.

\textbf{Teorema da Divergência}
\begin{equation}
  \label{eq:Tdivergencia}
  \oint_S\overrightarrow{F}.\widehat{n}dS = \oint_V\overrightarrow{\nabla}.\overrightarrow{F}dV
\end{equation}
Usando \ref{eq:Mtotal}, \ref{eq:Flxtotal} e \ref{eq:Flxtotal2} em \ref{eq:Tdivergencia}, obtemos a equação da continuidade, expressando a conservação de massa:
\begin{equation}
  \label{eq:Continuidade}
  \oint_V\left(\frac{\partial\chi}{\partial t} + \overrightarrow{\nabla}.\overrightarrow{F}\right)dV = 0 ,
\end{equation}
com
\begin{equation}
  \label{eq:diffConcPorTempo}
  \frac{\partial\chi}{\partial t} = -\overrightarrow{\nabla}.\overrightarrow{F} ,
\end{equation}
uma vez que não há advecção.
Ou seja, de acordo com a equação da continuidade, a variação da massa no tempo é igual ao fluxo total no volume, como esperado, se há conservação de massa.
\subsubsection{Equação da Difusão Molecular}
Substituindo \ref{eq:Fluxo} em \ref{eq:diffConcPorTempo}, obtemos:
\begin{equation}
  \label{eq:ConcTempoPreLaplace}
  \frac{\partial\chi}{\partial t} = \overrightarrow{\nabla}.(D\overrightarrow{\nabla}\chi)
\end{equation}
o que nos fornece a \textbf{Equação da Difusão Molecular} em seu formato usual:
\begin{equation}
  \label{eq:DifusaoMolecular}
  \frac{\partial\chi}{\partial t} = D\nabla^2\chi
\end{equation}

Se, além da difusão molecular, um fluxo laminar contribui com o fluxo total, reescrevemos \ref{eq:Fluxo} como:
\begin{equation}
  \label{eq:Flxvelocidade}
  \overrightarrow{F} = \overrightarrow{u}\chi -D\overrightarrow{\nabla}\chi
\end{equation}
Supondo, por enquanto, que temos fluidos incompressíveis,
\begin{equation}
  \label{eq:NablaUconc}
  \overrightarrow{\nabla}.(\overrightarrow{u}\chi)=  \overrightarrow{u}.\overrightarrow{\nabla}\chi
\end{equation}
e temos a equação da difusão envolvendo tanto advecção por vento quanto difusão molecular:
\textbf{Equação geral da difusão}
\begin{equation}
  \label{eq:DifusaoGeral}
  \frac{d}{dt}\chi = \frac{\partial\chi}{\partial t} + \overrightarrow{u}.\overrightarrow{\nabla}\chi = D\nabla^2\chi
\end{equation}
, onde $\frac{d}{dt} \equiv$ derivada total.

A solução para a equação da difusão molecular \ref{eq:DifusaoMolecular} é uma gaussiana, o que em uma dimensão fica:
\begin{equation}
  \label{eq:gauss1D}
  \chi = \frac{A}{\sqrt{t}}\exp\left(\frac{-x^2}{4Dt}\right)
  , A \equiv constante.
\end{equation}

A quantidade de material em uma coluna, na direção x pode ser calculada como:
\begin{equation}
  \label{eq:Q}
  Q = \int_{-\infty}^\infty \chi dx = \int_{-\infty}^\infty \exp{\frac{-x^2}{4Dt}} = 2A\sqrt{\pi D} ,
\end{equation}
de onde obtém-se a constante A e, substituindo este valore em \ref{eq:gauss1D}, temos:
\begin{equation}
  \label{eq:Gauss1D}
  \chi = \frac{Q}{2\sqrt{\pi Dt}}\exp\left(-\frac{x^2}{4Dt}\right)
\end{equation}

\begin{equation}
  \label{eq:Gauss1Dassintotico}
  \begin{split}
    Para\\
  \chi \rightarrow 0, |x| > 0\\
  \chi \rightarrow \infty, x = 0\\
  \end{split}
\end{equation}
O comportamento assintótico da solução \ref{eq:Gauss1D} mostrado em \ref{eq:Gauss1Dassintotico} evidencia seus caráter pontual, como uma função delta de Dirac.
Também possui um caráter instantâneo, quando $t \rightarrow 0 $.
\begin{equation}
  \label{eq:ConcDeltaDirac}
  \begin{split}
      \chi = Q\delta(x),\\
      t = 0\\
  \end{split}
\end{equation}

O desvio padrão para a concentração de $\chi$ pode ser dado por:
\begin{equation}
  \label{eq:DesvPadEuler}
  \sigma^² = \frac{1}{Q}\int_{-\infty}^\infty x^2\chi dx = 2Dt
\end{equation}
Substituindo \ref{eq:DesvPadEuler} em \ref{eq:Gauss1D}, temos:
\begin{equation}
  \label{eq:Gauss1Ddesvpad}
  \chi =\frac{Q} { \sqrt{2\pi}\sigma }\exp\left(\frac{-x^2}{2\sigma^2}\right)
\end{equation}

Podemos ter, ainda, uma forma normalizada: $\frac{\chi\sigma}{Q} = \frac{1}{\sqrt{2\pi}}\exp\left(\frac{-1}{2}\frac{x^2}{\sigma^2}\right)$

Como solução da equação de difusão geral \ref{eq:DifusaoGeral}, envolvendo tanto o vento médio na direção x quanto difusão molecular, aplicamos o método de separação de variáveis, e temos a equação \ref{eq:SolDifusaoGeral}
\begin{equation}
  \label{eq:SolDifusaoGeral}
  \chi = \frac{Q}{(\sqrt{2\pi}\sigma)^3}\exp\left(-\frac{(x-ut)^2 + y^2 + z^2}{2\sigma^2}\right)
\end{equation}
Com $\overrightarrow{u} = u\widehat{x}$, e $\sigma_x = \sigma_y = \sigma_z = \sigma$.

Levando em consideração que a troposfera tem cerca de 10Km de altura, e que a pluma se propaga muito mais na horizontal %não há direção horizontal - é o plano horizontal com as direções x e y
%
do que em z, podemos fazer a aproximação $z \ll x$. Em z, a gaussiana também não é simétrica, e podemos supor a reflexão das partículas que atingem o solo.
Para satisfazer as duas considerações, reescrevemos
\begin{equation}
  \label{eq:Zdesprezivel}
  r-x \simeq \frac{y^2 + z^2}{2x^2}
\end{equation}
E aplicamos a condição de reflexão $F_z(0) = 0$.
\begin{equation}
  \label{eq:CondRefexao}
  F_z = \left(-D\frac{\partial \chi}{\partial z}\right)_0 = 0
\end{equation}

Assim, temos a equação \ref{eq:Gauss3Dreflexao}, já integrada em relação ao tempo, de 0 a infinito, expressando uma emissão contínua no tempo, com q representando uma taxa de emissão de massa por unidade de tempo.
\begin{equation}
  \label{eq:Gauss3Dreflexao}
  \chi = \frac{q}{2\pi u\sigma^2}\exp\left(\frac{-y^2}{2\sigma^2}\right)\left(\exp\left(\frac{-(z-h)^2}{2\sigma^2}\right)+\exp\left(\frac{-(z+h)^2}{2\sigma^2}\right)\right),
\end{equation}
 onde reescrevemos $\sigma = \sqrt{2D}\frac{x}{u}$ e h é altura da fonte em relação ao solo.

\subsection{Difusão Molecular - Formulação estatística}
Seja uma partícula na posição $\overrightarrow{x'}$ em t=0.
$\overrightarrow{x} - \overrightarrow{x'}$ é o vetor deslocamento de $\overrightarrow{x'}$ a $\overrightarrow{x}$, dependendo do tempo de forma aleatória e descrito por uma função densidade de probabilidade $P(\overrightarrow{x} -\overrightarrow{x'} )dx \equiv$ probabilidade de que o vetor deslocamento terminará dentro do elemento dx em torno de $\overrightarrow{x}$. Uma outra interpretação possível é que $P(\overrightarrow{x} -\overrightarrow{x'})$ é a probabilidade de, no tempo t, $\overrightarrow{x}$ estar imerso no elemento difundido em questão.
Essa abordagem parte da suposição que o campo de difusão é homogêneo e P não depende explicitamente de $\overrightarrow{x'}$, sendo todos os pontos de lançamento equivalentes entre si.

A distribuição de densidade de partículas %Densidade do quê? Seria densidade de partículas, o que corresponde a uma concentração, como registra o Csanady%
   $\chi$ é dada por:
\begin{equation}
  \label{eq:distrDensidade}
  \chi (\overrightarrow{x},t|\overrightarrow{x'}) = QP(\overrightarrow{x} -\overrightarrow{x'})
\end{equation}

Supondo um processo estocástico estacionário,
\begin{equation}
  \label{eq:CondEstacionario}
  \begin{split}
      \overline{u(t)} = 0\\
      \overline{u^2}(t) = constante = \overline{u^2}\\
  \end{split} ,
\end{equation}

e relações similares valem para as demais componentes da velocidade.

\subsubsection{Teorema de Taylor}

Seja $R(\tau) \equiv$ a função de autocorrelação.

Para $t < \tau$, os valores de u são similares.
$R(\tau)$ mede a \"persistência\"    %tirei as barras das aspas para ela não virarem trema, como aparecia no meu texto%
 de um dado valor de uma variável aleatória.
Seja $\overline{u(t)u(t+\tau)} \equiv$ covariança de velocidade. Para $\tau \rightarrow 0$, ela tende a $\overline{u^2(t)}$.
Para um tempo $\tau$ suficientemente grande, $u(t)$ e $u(t+ \tau)$ variam independentemente e  $\overline{u(t)u(t+ \tau)}$ representa a média sobre uma variável aleatória, o que é 0 por hipótese.
Para um processo estacionário, a covariança da velocidade tem, como propriedade , o fato de que é independente de t escolhido como base de cálculo, ou seja:
\begin{equation}
  \label{eq:covarVel}
  \begin{split}
      \overline{u(t)u(t + \tau)} = \overline{u(0)u(\tau)} = \overline{u(-\tau)u(0)} = ... etc
  \end{split}
\end{equation}
Assim, $\overline{u(t)u(t+\tau)}$ é uma função par de $\tau$.

A função de autocorrelação, adimensional, pode ser definida como:
\begin{equation}
  \label{eq:autofuncCorr}
  R(\tau) = \frac{\overline{u(t)u(t+\tau)}}{\overline{u^2}}
\end{equation}
Se $\tau \rightarrow 0$, $R \rightarrow 1$, se $\tau \rightarrow \infty$, $R \rightarrow 0$ e
\begin{equation}
  \label{eq:ValorR}
  -1 \leq R(\tau) \leq 1, \forall \tau
\end{equation}

A equação de posição para qualquer partícula no conjunto é:
\begin{equation}
  \label{eq:EqHoraria}
  x(t) = \int_0^t u(t')dt'\  ,
\end{equation}
o que nos permite escrever:
\begin{equation}
  \label{eq:PreTaylor}
  \frac{d}{dt}x^2(t) = 2x\frac{d}{dt}x = 2\int_0^t u(t)u(t')dt'
\end{equation}

Usando \ref{eq:autofuncCorr}, e substituindo em \ref{eq:PreTaylor}, chegamos a:
\begin{equation}
  \label{eq:Taylor}
  \frac{d}{dt}\overline{x^2} = 2\overline{u^2}\int_0^tR(\tau)d\tau \  ,
\end{equation}
onde foi usado o fato de que $R(\tau)$ é par para transformar a variável de integração de t' para $\tau = t'-t$.

A equação \ref{eq:Taylor} é o Teorema de Taylor \citep{taylor1922diffusion}, como é conhecido na difusão turbulenta. Taylor estudou a difusão turbulenta baseado na teoria estatística do movimento Browniano.

Vejamos como esse resultado pode ser usado para um conjunto de partículas lançado em $\overrightarrow{x} = 0$ e em t=0. O desvio padrão é:
\begin{equation}
  \label{eq:desvpadStat}
  \sigma_x^2 = \frac{1}{Q}\iiint x^2\chi (x,y,z,t)dxdydz,
\end{equation}
 que é uma boa medida da dispersão desta nuvem de partículas.

Usando \ref{eq:distrDensidade}, pode ser definido também como:
\begin{equation}
  \label{eq:desvpadStat2}
  \sigma_x^2 = \iiint x^2 P(x,y,z,t)dxdydz = \overline{x^2},
\end{equation}
 e pode ser calculado a partir do teorema de Taylor, se houver informação sobre a história da partícula em difusão.
Usando \ref{eq:Taylor},
\begin{equation}
  \label{eq:PredesvpadStatTaylor}
  \frac{d}{dt}\sigma_x^2 = 2\overline{u^2}\int_0^tR(\tau)d\tau
\end{equation}
Integrando \ref{eq:PredesvpadStatTaylor} no tempo:
\begin{equation}
  \label{eq:desvpadStatTaylor}
  \sigma_x^2 = 2\overline{u^2}\int_o^t\int_o^{t'} R(\tau)d\tau dt' = 2\overline{u^2}\int_0^{t}(t-\tau)R(\tau)d\tau ,
\end{equation}
integrando em t' antes, e sabendo que $t' = t-\tau$

\subsubsection{Movimento Browniano}
% Para mim(ak): avaliar se vale a pena extender-se sobre esse ponto. Verificar a conexão dele com o objeto mais central da dissertação.
Hipóteses:
\begin{itemize}
  \item 1) A partícula é muito maior que as moléculas do fluido. Nela, atua a força de viscosidade $-\beta \overrightarrow{u}$, $[\beta] = s^{-1}$;%
  %Akjl/2017 esta igualdade para beta está estranha. Talvez colocar "beta em s-1".
  %
  \item 2) A partícula é pequena o bastante para ser afetada pelas colisões moleculares.
\end{itemize}

Assim, a outra força a afetar a partícula é aleatória (estocástica), de modo que a equação do movimento Browniano é uma \textbf{Equação de Langevin}.
\begin{equation}
  \label{eq:Langevin}
  \frac{d}{dt}\overrightarrow{u} = -\beta\overrightarrow{u} + \overrightarrow{A}(t),
\end{equation}
 onde $\overrightarrow{A(t)}$ é uma aceleração estocástica.

Para uma partícula esférica, $\beta$, o coeficiente de viscosidade pode ser definido pela Lei de Stokes abaixo:
\begin{equation}
  \label{eq:LeiStokes}
  \beta = 6\pi a\mu/m, [s^-1],
\end{equation}
 onde a é o raio da partícula, $\mu$ é a viscosidade do fluído e m é a massa da partícula.
$\beta^{-1}$ é o tempo de relaxação.

A solução homogênea de \ref{eq:Langevin} é $\overrightarrow{u} = \overrightarrow{u_o}\exp(-\beta t)$, com $\overrightarrow{u_o} = \overrightarrow{u}(t=0)$. A solução geral é:
\begin{equation}
  \label{eq:SolLangevin}
  \overrightarrow{u} = \overrightarrow{u_0}\exp(-\beta t) + \exp(-\beta t)\int \exp(+\beta t)\overrightarrow{A(t')}dt'
\end{equation}

Nota-se que para $t \gg \beta^{-1}$, a partícula ``esquece'' sua velocidade inicial, ficando apenas com o termo estocástico.

\paragraph{Dispersão de partículas Brownianas}
% Essa formulação precisa ser melhor precisada, já que a energia da partícula está derivando de uma das componentes da velocidade média das moléculas do gás onde as partículas estão imersas.
A energia cinética das partículas que se difundem em um gás é resultado da energia cinética média das moléculas do gás onde estão imersas, portanto, observando apenas uma direção, temos:
\begin{equation}
  \label{eq:Ecinetica}
  \frac{1}{2}m\overline{u^ 2} = \frac{1}{2}\kappa T,
\end{equation}
 onde $\kappa$ é a constante de Boltzmann e T a temperatura absoluta.

Partindo de \ref{eq:SolLangevin}, pode-se achar a velocidade de covariança e a autofunção de correlação:
\begin{equation}
  \label{eq:autoCorrFunc2}
  R(\tau) = \frac{\overline{u(0)u(\tau)}}{\overline{u^2}} = \exp(-\beta t)
\end{equation}

Substituindo no teorema de Taylor, temos:
\begin{equation}
  \label{eq:PredesvpadStatBrownian}
  \sigma_x^2 = 2\overline{u^2}\left[\frac{t}{\beta} -\frac{-1}{\beta}(1 - \exp(-\beta t))\right]
\end{equation}

Para $t \gg \beta^{-1}$, 
\begin{equation}
  \label{eq:desvpadStatBrownian}
  \sigma_x^2 = \frac{2\overline{u^2}}{\beta}t
\end{equation}

Comparando com o desvio padrão da solução para a  equação de difusão clássica \ref{eq:DesvPadEuler}, A difusão molecular pode, agora, ser definida em termos estatísticos.
\begin{equation}
  \label{eq:ConstDifusao}
  D = \frac{\overline{u^2}}{\beta}
\end{equation}

e, para partículas esféricas no movimento Browniano, usando \ref{eq:LeiStokes} e \ref{eq:Ecinetica}, temos 
\begin{equation}
  \label{eq:DparticulaEsferica}
  D = \frac{\kappa T}{6\pi a \mu}
\end{equation}

\paragraph{Modelo do Simples Caminho Aleatório}

Com $\Delta t \gg \beta^{-1}$, entre uma colisão e outra da partícula com as moléculas do meio, o movimento da partícula é governada, basicamente, por passos aleatórios.
O Deslocamento $x_j$ que a partícula executa em cada passo, para cada intervalo de tempo é:
\begin{equation}
  \label{eq:RandomXj}
  x_j = \int_{j\Delta t}^{(j+1)\Delta t} u(t')dt'
\end{equation}

No caminho aleatório, a partícula, após N passos, pode estar em quaisquer dessas posições

$-N, -N +1, -N+2, ..., -1, 0, +1, ...N-1, N,$

se a probabilidade de ir para frente ou para traz for $\frac{1}{2}$ (50\%), e a partir da origem.
Seja P(m,N), a probabilidade da partícula atingir $-N < \mathbf{m} < N$, após N passos.
Obtêm-se enumerando-se todos os possíveis resultados de um passo aleatório feito de N passos e determinando quais destes farão a partícula atingir \textbf{m}.
Cada passo tem probabilidade $\frac{1}{2}$, então cada sequência individual tem probabilidade $(\frac{1}{2})^N$ dos passos serem todos independentes.

Seja o número de passos para frente f (forward), e o número de passos para trás b (backward), então:
\begin{equation*}
  \begin{split}
    f + b = N \\     
    f - b = m
  \end{split}
\end{equation*}%convém colocar cada equação dessas em uma linha separada.
%
Obtém-se que $f = \frac{N+m}{2}$  e  $b=\frac{N-m}{2}$.
Da teoria de análise combinatória, o número de sequências diferentes de exatamente f passos para frente e b passos para trás é $\frac{N!}{f!b!}$.

Assim, obtém-se uma distribuição binomial, que também é uma distribuição de Bernoulli, pois P(m,N) é o \textbf{\emph{número de sequências distintas que levarão a m}, vezes a \emph{probabilidade dos passos serem todos independentes}}:

\begin{equation}
  \label{eq:Probabilidade1}
  P(m,N) = \frac{N!}{[\frac{1}{2}(N+m)]![\frac{1}{2}(N-m)]!}\left(\frac{1}{2}\right)^N,
\end{equation}
Onde N e m tem de ser ambos par ou ímpar.

Por ser uma distribuição de Bernoulli, ela tem as seguintes propriedades:
\begin{equation}
  \label{eq:BernoulliPropriedades}
  \begin{split}
    \overline{m} = \sum_{m = -N}^{N} mP(m,N) = 0\\
    \overline{m^2} = \sum_{m=-N}^{N} m^2P(m,N) = N\\
  \end{split}
\end{equation}

No movimento Browniano, até por sua formulação estatística, o interesse na análise é para $N \gg 1$.

Nestas condições, de acordo com o teorema de De Moivre-Laplace \citep{feller1968introduction-stats}, a distribuição binomial para um evento
de probabilidade de sucesso converge para uma distribuição gaussiana.

Assim, a distribuição binomial torna-se:
\begin{equation}
  \label{eq:Probabilidade2}
  P(m,N) = \sqrt{\frac{2}{\pi N}}\exp\left(\frac{-m^2}{2N}\right)
\end{equation}

A distribuição discreta pode ser considerada contínua se temos passo pequenos comparados com o comprimento $\Delta x$ em que se define a concentração de partícula.

Assim,
\begin{equation}
  \label{eq:TamanhoPasso}
  m = \frac{x}{l},
\end{equation}
 onde  m é o número de passos, l é o comprimento do passo, e x é o deslocamento da partícula a partir da origem.

$P(m,N).\frac{\Delta x}{2l} \equiv$ Probabilidade de se encontrar a partícula numa região $\Delta x$ (centrada em x).
O material contido na região $\Delta x$ é:
\begin{equation}
  \label{eq:DeltaM}
  \Delta M = QP(m,N)\frac{\Delta x}{2l},
\end{equation}
 e a distribuição da concentração torna-se:
\begin{equation}
  \label{eq:distrConc}
  \chi = \frac{\Delta M}{\Delta x} = \frac{Q}{2l}\sqrt{\frac{2}{\pi N}}\exp\left(-\frac{x^2}{2Nl^2}\right)
\end{equation}
O número total de passos está associado ao tempo de difusão. Se a partícula faz n deslocamentos por unidade de tempo, $t = \frac{N}{n}$, e $u = \frac{1}{2}nl$ é um tipo de \"velocidade de difusão\".

Assim,
\begin{equation}
  \label{eq:DifusaoRandom}
  D = \frac{1}{2}nl^2 = ul
\end{equation}

e a distribuição da concentração torna-se,
\begin{equation}
  \label{eq:7}
  \chi(x,t) = \frac{Q}{2\sqrt{\pi Dt}}\exp\left(-\frac{x^2}{4Dt}\right),
\end{equation}
ou seja, o mesmo resultado encontrado para a difusão clássica formulada na equação \ref{eq:Gauss1D}%Como comentei no início, precisa avaliar o que será apresentado nesta parte teórica. Você não está escrevendo um livro. Às vezes é melhor uma explicação mais genérica sobre como algo é demonstrado e qual seu resultado, do que meia dedução, que abre espaço para ser perguntado sobre ela. Você domina essa dedução? O que você será mais cobrado é sobre o equacionamento terminal para o SPRAY. O que fez aqui é o desenvolvimento estatístico de uma gaussiana, que é bem deduzido no livro do Otaviano&Vito. Poderia até usá-lo como citação para uma abordagem mais simplificada.
%
%O desenvolvimento a seguir não está casando com o que fez até agora e você não chega ao ponto de conectar o que fez com naquilo que usa o SPRAY.
% Removi uma parte da dedução e citei fonte para pulá-las. (Atenágoras)

\subsection{Difusão Turbulenta}
A difusão turbulenta é um processo que tem uma componente determinística e outra estocástica, obedecendo, portanto, a Equação de Langevin.

Assim, o desenvolvimento teórico para a difusão molecular pelo movimento Browniano é empregado para o estudo do movimento de partículas na atmosfera, até porque processos estocásticos, em geral, sugerem a possibilidade de uso de uma distribuição gaussiana. Entretanto, o tempo de escala lagrangiano $T_L$ é maior que o de $\beta^{-1}$ usado na difusão molecular.

São assumidas duas hipóteses para este desenvolvimento:
\begin{itemize}
\item 1) A turbulência é homogênea no espaço
\item 2) A turbulência é estática
\end{itemize}
\subsubsection{Equação de Langevin para Turbulência}
\begin{equation}
  \label{eq:LangevinTurbulencia}
  \frac{d}{dt}u' = -\frac{1}{T_L}u'(t) + \eta(t)
\end{equation}
, onde $T_{Li}$, $i = u',v',w'$ é a escala de tempo Lagrangiana, que, assim como $\beta^{-1}$ está associado ao tempo em que a correlação da componente determinística da turbulência ``desaparece''. u', v' e w' são componentes da velocidade turbulenta em x, y e z.

Embora na atmosfera as partículas também estejam sofrendo difusão molecular, esta é praticamente desprezível, frente à difusão turbulenta, então $\sigma_x$ é exclusivamente turbulento no modelo, assim como os desvios padrões para as demais componentes.

A função de autocorrelação segue desenvolvimento análogo ao que foi feito para a difusão molecular, portanto:
\begin{equation}
  \label{eq:AutoFuncCorrTurb}
  R(\tau) = \exp\left(-\frac{\tau}{T_{Li}}\right)
\end{equation}

\subsubsection{A Forma Gaussiana da Distribuição de Concentração para Difusão Turbulenta}
Embora não haja nenhuma prova formal, através de experimentos e observações como os do Pasquill \citep{slade1968meteorology}, verifica-se que a distribuição média da concentração de partículas na atmosfera pode ser representada por uma gaussiana em condições aproximadas como homogêneas e estacionárias.

Com estas hipóteses assumidas para a difusão turbulenta verificadas nestes experimentos (em breves momentos e em condições espaciais específicas), verifica-se que os desvios padrões são os mesmos que os esperados de uma distribuição gaussiana, com $\sigma_{xi} = \overline{x_i^2}$, $x_i = x,y,z$ e $\overline{x} = \overline{y} = \overline{z} = 0$.

Para $t \ll T_L$, P(x,y,z) é aproximadamente gaussiana, porque é proporcional à distribuição de velocidade turbulenta de cada componente, que é gaussiana.

Além disso, $\overline{xy} = \overline{yz} = \overline {xz} = 0$.

Assim, a forma da solução para a distribuição de concentração de poluentes é:
\begin{equation}
  \label{eq:ConcTurb}
  \chi(x,y,z,t) = \frac{Q}{(2\pi)^{2/3}\sigma_x\sigma_y\sigma_z}\exp\left[-\frac{(x-Ut)^2}{2\sigma_x^2} -\frac{y^2}{2\sigma_y^2} -\frac{z^2}{2\sigma_z^2}  \right],
\end{equation}
onde $ U = \overline{\overrightarrow{U}}$.
Integrando no tempo, assumindo que $z \ll x$, e a condição de reflexão em z, expressa em \ref{eq:CondRefexao}, como foi feito em \ref{eq:Gauss3Dreflexao}, chegamos à Distribuição Gaussiana Para Emissão Contínua:
\begin{equation}
  \label{eq:ConcTurbReflexao}
  \chi(x,y,z) = \frac{q}{2\pi U\sigma_y\sigma_z}\exp\left( -\frac{y^2}{2\sigma_y^2}\right)
  \left[ \exp\left( -\frac{(z-h)^2}{2\sigma_z^2} \right)+ \exp\left( -\frac{(z+h)^2}{2\sigma_z^2} \right) \right],
\end{equation}
q é a taxa de emissão de massa por unidade de tempo.

\section{Modelagem Numérica da Dispersão de Partículas no SPRAY }
\label{sec:teoriaSPRAY}

Até aqui, foi mostrado o desenvolvimento tanto euleriano quanto estocástico da solução da concentração de partículas, 
inicialmente no movimento Browniano, e depois de uma breve discussão, sua razoabilidade para a difusão turbulenta na atmosfera, quando, em condições homogêneas e estacionárias, esta solução é gaussiana.

Entretanto, a atmosfera não apresenta turbulência homogênea e estacionária, além do próprio vento médio variar sua direção e intensidade com o tempo.

Assume-se que as partículas são carregadas pelo vento, possuindo, ambos, a mesma, velocidade.

As emissões de uma fonte são divididas em partículas virtuais.

O SPRAY, a partir de informações inseridas pelo modelo meteorológico, a fonte, e configurações ajustadas em arquivos de inicialização, calcula a trajetória de cada partícula emitida a cada intervalo de tempo.

As informações de entrada oriundas do modelo meteorológico fornecem a velocidade do vento em diferentes níveis de altura, a topografia do terreno e demais variáveis necessárias para o cálculo da turbulência nas várias equações do modelo: $z_o$, $u_*$, L, h, $w*$ e $h_{res}$, respectivamente, rugosidade do solo, velocidade de fricção, cumprimento de Monin-Obukhov, altura da camada de mistura, escala vertical de velocidades convectivas e altura da camada de mistura residual do dia anterior.

A partir desses parâmetros, são calculados $\sigma_{u_i}$, i = x,y,z,  $\overline{w^3}$ (obliquidade da curva de distribuição de w),  $T_{L_i}$.

A trajetória de uma partícula pode ser integrada a partir da equação:

\begin{equation}
  \label{eq:particula-trajeto}
  \overrightarrow{x(t+dt)} = \overrightarrow{x(t)} + \overrightarrow{U(x(t),t)dt},
\end{equation}
onde $\overrightarrow{x}$ é a trajetória da partícula, $\overrightarrow{U(x(t),t)}$ é a velocidade da partícula 
(na verdade, a velocidade do vento carregando a partícula), e $dt$ é o intervalo de tempo para a integração discreta.

$\overrightarrow{U}$ é dividida em duas componentes: Uma velocidade média determinística, e outra turbulenta:
\begin{equation}
  \label{eq:velocidade-componente}
  \overrightarrow{U} = \overrightarrow{\overline{U}} + \overrightarrow{U'},
\end{equation}
onde $\overrightarrow{\overline{U}}$ é a velocidade média, e $\overrightarrow{U'}$ é a velocidade turbulenta.

Para tentar obter a velocidade turbulenta, são resolvidas equações de Langevin mais complexas, baseadas no esquema não-linear, descrito em \citet{thomson1987criteria}, levando em consideração o fato da atmosfera não ser, na verdade, nem homogênea nem estacionária. Baseando-se em ajustes empíricos sobre parâmetros meteorológicos que permitem definir a turbulência na atmosfera, determinam-se os $\sigma_{ui}$ e $T_{L_i}$ que determinarão as características da Função de Distribuição de Probabilidade (PDF) para o cálculo de U'. Na horizontal utiliza-se PDFs gaussianas, enquanto na vertical, é necessário utilizar uma função densidade de probabilidade não-gaussiana, usualmente uma Bi-Gaussiana ou Gram-Charlier truncada na 3ª ou 4ª ordem.

Neste trabalho, foi utilizada uma função oriunda da expansão das séries de Gram-Charlier truncada na 3ª ordem.

A expansão Gram-Charlier de 4ª ordem tem a forma:
\begin{equation}
  \label{eq:Gram-Charlier4th-ordem}
  P(x) = \frac{\exp^{-\frac{x^2}{2}}}{\sqrt{2\pi}}(1 + C_3H_3 +C4_4),
\end{equation}
onde $H_3 = x^3 - 3x$, $H_4 = x^4 -6x + 3$ são os polinômio de Hermite,  $C_3 = \frac{\overline{\mu^3}}{6}$, $C_4 = \frac{\overline{\mu^4}-3}{24}$ são constantes, $\overline{\mu^3}$ e $\overline{\mu^4}$ são momentos padronizados de w, e $x = \frac{w}{\sigma_w}$.

Quando $C_4=0$, a expansão está truncada na 3ª ordem.


% Não esquecer de ir no Manual do SPRAY, pegar as fontes para os artigos e citá-las.

% Tentar Compilar
% Passar o capítulo 3 do Csanady (rápido).

% AucTeX
% C-c C-s insere uma sessão, capítulo etc. TAB faz completion e mostra as opções disponíveis
% C-c C-e insere um ambiente (figure, equation etc). Pode-se usar TAB
% C-c C-m insere uma macro, (ref, cite etc). Pode-se usar TAB~

% Comandos refTex
% chamar referências: C-c ). Depois apertar enter e escolher o tipo e a referência
% citações: C-c [. Depois, é só chamar digitar alguma coisa, e usar completion. Por enquanto, só funciona no arquivo principal

% Informando ao emacs/AucTex/RefTex quem é o arquivo-master da dissertação

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../dissertacao"
%%% End: