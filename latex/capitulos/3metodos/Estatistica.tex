\section{Métodos de avaliação dos resultados simulados}
\label{sec:Estatistica}
Uma vez de posse dos resultados da simulação e dos dados observados nas estações meteorológicas, foram calculados índices e parâmetros para avaliar a confiabilidade estatística deles e a performance da simulação como um todo.

Para avaliar a confiabilidade estatística da simulação, foi calculada a correlação (R) entre valores simulados e observados, e feito o teste de hipótese nula.

 (ou seja, no caso, a hipótese dos valores simulados e observados estarem correlacionados) para um nível de significância $\alpha \leqslant 0,05$. No caso da simulação de vento, foram calculadas $R_u$ e $R_v$, correlação das componentes zonal e meridional, respectivamente.
  Como as correlações zonal e meridional em cada estação são independentes, consideramos que a correlação mais baixa deve ser utilizada para  definir a confiabilidade do modelo em cada estação.

A Correlação foi calculada como:

\begin{equation}
  \label{eq:correlacao}
  R = \frac{ \overline{(C_{obs} - \overline{C_{obs}})(C_{sim} - \overline{C_{sim}})} }{\sigma_{C_{sim}}\sigma_{C_{obs}}},
\end{equation}
onde $C_{obs}$ é a concentração observada, $C_{sim}$ a concentração simulada, $\sigma_{sim}$ e $\sigma_{obs}$ são, respectivamente, o desvio padrão da concentração simulada e observada, respectivamente.

Para avaliar a performance da simulação foram calculadas grandezas como média dos valores simulados ($\overline{sim}$) e observados ($\overline{obs}$), razão entre estes ($\frac{\overline{sim}}{\overline{obs}}$), desvios padrões destes ($\sigma_{sim}$ e $\sigma_{obs}$), respectivamente, o RMSE (Root Mean Square Error - Raiz do Erro Quadrático Médio) e o NRMSE (Normalized Root Mean Square Error - Raiz do Erro Quadrático Médio Normalizado). Para as comparações entre vento simulado e observado, foram utilizados o RMSVE (Root Mean Square Vector Error - Raiz do Erro Vetorial Quadrático Médio) e o desvio padrão $\sigma(u,v) = \sqrt{\sigma^{2}_u+\sigma^{2}_v}$.

Para o cálculo do RMSE, utilizamos:

\begin{equation}
  \label{eq:RMSE}
  RMSE = \sqrt{\overline{(C_{sim}-C_{obs})^2}}
\end{equation}

O NRMSE foi calculado como $NRMSE = 100 \frac{RMSE}{\sigma_{obs}}$ .

O RMSVE para o vento foi calculado como:

\begin{equation}
  \label{eq:RMSVE}
  RMSVE = \sqrt{\overline{(u_{sim} - u_{obs})^2 + (v_{sim} - v_{obs})^2}},
\end{equation}
onde u e v são as velocidades zonal e meridional, respectivamente. Os índices sim e obs representam simulado e observado, respectivamente.

O NRMSVE foi calculado como $NRMSVE = 100\frac{RMSVE}{\sigma(u,v)_{obs}}$, onde $\sigma(u,v)_{obs}$ é o desvio padrão da magnitude do vento observado.

Os valores ideais destes parâmetros são:
\begin{itemize}
  \item $\overline{sim} = \overline{obs}$. Mostra perfeita acurácia da simulação;
  \item $\frac{\overline{sim}}{\overline{obs}} = 1$. Consequência direta do item acima;
  \item $\sigma_{sim} \approx %\leqslant
     \sigma_{obs}$. O Desvio Padrão dos valores simulados deve ser similar (idealmente igual) %menor ou igual%
    ao dos valores observados, mostrando que esses dois conjuntos de valores têm dispersões similares, o que indicaria um comportamento do modelo semelhante à natureza, à física ou leis do fenômeno estudado.
  \item RMSE = 0 (ou NRMSE = 0). A Raiz do Erro Quadrático Médio, ou a Raiz do Erro Quadrático Médio Normalizado, indica o quão distante estão os valores simulados em relação aos medidos. Tipicamente espera-se que RMSE seja próximo de $\sigma_{obs}$. NRMSE ajuda a informar quão distante está o conjunto de dados simulados do conjunto de dados observados em termos de $\sigma_{obs}$. %  está a simulação da média dos valores observados%. %Comentei esse trecho porque o RMSE pode ser grande e as médias podem ser iguais.
  % 
\end{itemize}

\citet{HannaChang2004}, \citet{Kumar1990} e \citet{Kumar1993} apresentam também outros parâmetros, largamente utilizados na comparação de modelos de dispersão de poluentes e que podem, sempre que possível, ser utilizados para outras variáveis. Entre os parâmetros estatísticos apresentados, este trabalho se utilizou de FAC2 (Factor of two - Fator 2), que é a fração dos valores simulados dentro de um fator dois dos valores observados ($ 0,5 \leqslant \frac{SIM}{OBS} \leqslant 2$), FB (Fractional Bias - Viés Fracional), que mede o erro sistemático da simulação que tende a sempre superestimar ou subestimar os valores medidos e NMSE (Normalized Mean Square Error - Erro Quadrático Médio Normalizado), que mede tanto o espalhamento, e reflete tanto erros sistemáticos quanto aleatórios.

O cálculo para o FB se dá como:

\begin{equation}
  \label{eq:FractionalBias}
  FB = 2\frac{\overline{C_{obs}-\overline{C_{sim}}}}{\overline{C_{obs}}+\overline{C_{sim}}}
\end{equation}

O NMSE apresenta-se como:

\begin{equation}
  \label{eq:NMSE}
  NMSE = \frac{ \overline{(C_{obs} - C_{sim})^2} }{ \overline{C_{obs}}\ \overline{C_{sim}}}
\end{equation}

Os valores ideais destes parâmetros são:
\begin{itemize}
  \item FAC2 = 1;
  \item FB = 0;
  \item NMSE = 0.
\end{itemize}

Vários parâmetros estatísticos de performance e confiança do modelo devem ser utilizados para avaliá-lo porque todos apresentam vantagens e desvantagens, não podendo ser universalmente aplicados.

NMSE, por exemplo, pode ser influenciado por pontos de simulação muito acima dos valores observados \citep{NMSEpoli1993}, e FB pode ser zero, mas por conter vieses negativos e positivos que se cancelam, mas que um gráfico de espalhamento mostraria que os valores estariam distantes da ``reta 1 para 1'', indicativa da concordância entre os dados simulados e observados.
A melhor medida, geralmente, é o FAC2, por não ser influenciada por ``outliers'' como o NMSE, nem pelos cancelamentos de  vieses positivos ou negativos.
FB, mede vieses relativos médios, e indicam erros sistemáticos. Valores negativos, indicam que a simulação tende a superestimar as variáveis em relação aos dados observados.
NMSE mede o espalhamento relativo médio, além de medir tanto erros sistemáticos quanto aleatórios.

\subsection{Critérios de realidade para avaliar a performance dos modelos}
\label{sec:CriteriosAvaliacao}

A performance dos modelos dependem de muitos fatores: resolução das condições de contorno de entrada dos modelos para várias variáveis, incompletude do modelo em simular a natureza, ou das leis que governam os fenômenos estudados, erros na modelagem e/ou nas informações de grade que alimentam o modelo antes de começarem a rodar, erros na leitura dos dados observados, além de incertezas sobre as próprias leis e fórmulas que regem o fenômeno (até hoje, não se provou categoricamente a natureza gaussiana da turbulência atmosférica, ou a a microfísica de formação de nuvens sem recorrer a parametrizações experimentais, por exemplo) etc. Tais fatores tornam complicado estabelecer critérios rígidos para a comparação e validação da simulação com os dados observados.

Em \citet{HannaChang2004}, temos o estabelecimento de alguns critérios a partir da análise de muitos experimentos de comparação de modelos.
Geralmente, os critérios para aceitação dos modelos quanto à performance são (não mutuamente excludentes):
\begin{itemize}
  \item $FAC2 \geqslant 0,5$;
  \item $|FB| \leqslant 0,3$;
  \item $NMSE \leqslant 1,5$
\end{itemize}

Entretanto, a maioria das avaliações são de  comparações com experimentos controlados, e poucas dentre elas feitas com estações de monitoramento (exemplos mais reais, como o realizado neste trabalho).
Cisalhamento do vento, frentes e chuva, coisas associadas com ``tempo real'' podem causar deslocamento na posição das concentrações simuladas e observadas (nuvens de poluição) \citep{klug1992evaluation}.

Um espalhamento aleatório da média dentro de um fator 2 ou 3 também é aceitável.
\citet{Kumar1990} trabalharam com lançamentos instantâneos e pontuais de gases densos, portanto, um experimento controlado, e trabalhram com um FAC2 mais rigoroso: $FAC2 \geqslant 0,8 $.

Em \citet{BOOKAirPollutionModelling}, há vários experimentos controlados, em que se preferiu, mesmo nestas circunstâncias mais simples, utilizar $ FAC2 \geqslant 0,4$, flexibilizando este fator.

Em \citet{HannaChangBook}, verifca-se que é aceitável, para ambientes urbanos, estabelecer como critérios:
\begin{itemize}
  \item $|FB| \leqslant 0,67$, o que equivale a viés médio menor que ou igual a um fator 2;
  \item $NMSE \leqslant 6$, ou um espalhamento aleatório menor que 2,45 da média;
  \item \textbf{$FAC2 \geqslant 0,3$}, ou 30\% da simulação dentro de um fator 2.%não entendi os dois primeiros itens% O entendimento fica agora imediato fazendo sqrt(6), dada a fórmula para NMSE.
  %
\end{itemize}

A razão desta relaxação deve-se ao fato de que não é esperado que os modelos se comportem tão bem devido a existência de prédios, ou topografia muito acidentada e incertezas nas fontes quanto a suas resolução, posicionamento ou intensidade. Ainda, se a simulação for bem em  50\% dos parâmetros de performance, é lícito validá-la.

Levando em consideração que uma fonte de CO de resolução de $5X5Km^2$, quando a resolução da grade é de $3X3Km^2$, e da utilização de uma fontes para $MP_{10}$ cujas posições são estimadas na maioria das fábricas (exceto fábricas de fertilizantes, cuja localização é mais precisa) em Cubatão, foi razoável utilizar os seguintes parâmetros para analisar a simulação.
\begin{itemize}
  \item \textbf{$FAC2 \geqslant 0,3$};
  \item $|FB| \leqslant 0,67$, o que equivale a uma subestimativa ou superestimativa sistemática de cerca de dois terços em relação aos valores observados;
  \item $NMSE \leqslant 6$, o que equivale a uma espalhamento aleatório de aproximadamente 2,4;
  \item $NRMSE \leqslant 300\%$, o que equivale a uma distância da nuvem de valores simulados de no máximo $3\sigma_{obs}$ da nuvens de valores observados;
  \item $\sigma_{sim} \approx \sigma_{obs}$;%já comentei acima que esse é um bom critério, indicando que as distribuições de valores simulados e observados são iguais.%
  %
  \item R, com nivel de significância $p \leqslant \alpha = 0,05$.
\end{itemize}

Para as variáveis meteorológicas, FB, NMSE e FAC2 foram também calculadas quando foi possível (geralmente variáveis escalares, quando não foram apresentados valores negativos), e considerados caso a caso, conforme a necessidade, mas os parâmetros e critérios principais para analisá-las são o $NRMSE \leqslant 300\%$, $\sigma_{sim} \approx \sigma_{obs}$ e R, com nível de significância $\alpha \leqslant 0,05$.

Se a simulação apresentar resultados satisfatórios em mais de 50\% dos parâmetros, ela foi considerada validada para a experiência, desde que $p \leqslant \alpha = 0,05$, levando em consideração que isto significa que simulação e observação não estão correlacionados.

Todo o processamento estatístico realizado para a comparação da simulação com os dados observados foram realizados com o software R \citep{R}, e os pacotes hydroGOF \citep{hydroGOF} e Open Air \citep{openair}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../dissertacao"
%%% End: 
