\section{BRAMS}
\label{sec:brams}
O BRAMS, Brazilian Regional Atmospheric Modeling System, é um modelo de simulação meteorológica de mesoescala desenvolvido em conjunto pelo IAG (Instituto Astronômico e Geofísico), IME (Instituto de Matemática e Estatística), ambos unidades da USP (Universidade de São Paulo), pelo CPTEC/INPE (Centro de Previsão de Tempo e Estudos Climáticos/Instituto Nacional de Pesquisas Espaciais) e pela ATMET \citep{BRAMS2013}.
Suas raízes encontram-se no RAMS (Regional Atmospheric Modeling System), desenvovido originalmente na Colorado State University \citep{RAMSPielke} \citep{RAMScotton}.
O BRAMS adiciona correções e funcionalidades para que o modelo simule melhor a meteorologia na América do Sul.
Tendo como entrada informações sobre a vegetação, uso do solo, topografia, temperatura da superfície dos oceanos, e reanálises globais das variáveis meteorológicas fornecidas pelo CPTEC, parametrizações sobre formação de nuvens e microfísica, o BRAMS resolve as equações diferencias que governam o movimento na atmosfera, bem como equações termodinâmicas \citep{RAMSatmet} \citep{BRAMS2013}, e geram como saída resultados prognósticos das variáveis meteorológicas, tais como campo de vento, temperatura, umidade, precipitação etc.
O BRAMS pode trabalhar com grades aninhadas com comunicação bidirecional entre elas, permitindo que as regiões de interesse sejam resolvidas com informações de entrada com melhor resolução do que as utilizadas para inicializar o modelo.
%Eventualmente, colocar um diagrama do funcionamento do BRAMS
\subsection{Configuração da simulação meteorológica}
\label{sec:BRAMSconfig}
Para inicializar o BRAMS, foram utilizados arquivos com reanalises globais do CPTEC para as variáveis meteorológicas com resolução de tempo de 12h, e 0,9375º de latitude e longitude, cobrindo uma área entre 90ºS a 40ºN de latitude e 120ºO a 50ºL de longitude, além de 28 níveis verticais.
Foram utilizadas 3 grades aninhadas, cujos limites e detalhes estão na tabela \ref{table:Grade}: A grade 1, abrangendo o estado de São Paulo, de 12Km de resolução, a grade 2, contendo a RMSP, a Baixada Santista e a Região Metropolitana de Campinas, de 3Km de resolução, e a grade 3, abrangendo a RMSP e a Baixada Santista, de 1Km de resolução, para analisar melhor a região de Cubatão, devido a topografia acidentada. A resolução de tempo utilizada foi de 2s, mas os resultados eram lançados para cada horário.
\input{tabelas/metodos/Grade}
\input{figuras/metodos/Mapa2Grades}
%Eventualmente, procurar meu artigo para o SIICUSP2013, com a descrição detalhada de todos os passos para configurar o BRAMS.

%Inserir mapa das grades utilizadas.
%Inserir tabela com as grades.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../dissertacao"
%%% End: 
