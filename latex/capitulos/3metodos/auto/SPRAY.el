(TeX-add-style-hook "SPRAY"
 (lambda ()
    (LaTeX-add-labels
     "eq:Conc"
     "eq:Fluxo"
     "Mtotal"
     "eq:Flxtotal"
     "eq:Flxtotal2"
     "eq:Tdivergencia"
     "eq:Continuidade"
     "eq:diffConcPorTempo"
     "eq:ConcTempoPreLaplace"
     "eq:DifusaoMolecular"
     "eq:Flxvelocidade"
     "eq:NablaUconc"
     "eq:DifusaoGeral")))

