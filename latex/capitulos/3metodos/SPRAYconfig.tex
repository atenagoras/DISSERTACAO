\section{SPRAY}
\label{sec:SPRAY}

O SPRAY foi o modelo de dispersão lagrangiano utilizado para simular a dispersão dos poluentes. Os parâmetros meteorológicos relevantes para sua operação foram obtidos das simulações feitas previamente com o BRAMS.
Os resultados do BRAMS precisam ser adaptados para assimilação no SPRAY, o que é feito pelo programa GAP (Grid Adaptator). Ele converte o sistema de coordenadas (lat/lon) para UTM, e interpola os dados para a grade que compõe a base de dados para o SPRAY. Essa base de dados ainda é previamente processada pelo programa SurfPRO, juntamente com informações de uso do solo da USGS (United States Geological Survey), estimando os parâmetros de escala da turbulência na camada limite que o SPRAY necessita seja capaz de gerar as componentes estocásticas do vento e, consequentemente do movimento das partículas emitidas.

O SPRAY foi desenvolvido pelo Centro Ricerca Ambiente e Materiali da ENEL (Ente Nazionale per Energia Elettrica) e pelo Istituto di Cosmo-Geofisica do Consiglio Nazionale di Richerca da Itália, e tem o seu desenvolvimento continuado pela empresa ARIANET.
Suas bases são descritas em \citet{SPRAY1994} e \citet{SPRAY2000}, além de ter a teoria pertinente para este experimento desenvolvida na sessão \ref{sec:teoriaSPRAY}.

\subsection{Tornando o acoplamento BRAMS/SPRAY operacional}
\label{sec:acoplamentoBRAMS-SPRAY}

Um dos primeiros objetivos deste projeto de mestrado, era tornar o acoplamento BRAMS/SPRAY operacional.

Qualquer modelo meteorológico de diagnóstico ou prognóstico pode ser usado para fornecer as informações meteorológicas necessárias para se operar o modelo SPRAY, compondo um sistema de modelos capaz de gerar campos tridimensionais de concentração de poluentes. Basta que se proveja uma interface de comunicação para a transferência destas informações, que seguem acompanhadas da topografia sobre a qual o sistema meteorológico trabalhou. O SPRAY conta com interfaces para os códigos do SWIFT (diagnóstico) \citep{tech2010SWIFT}, WRF \citep{michalakes2001WRF} e RAMS (prognósticos).

No contexto da análise pretendida da dispersão de poluentes da RMSP e das Fábricas de fertilizantes de Cubatão, pretendia-se também atualizar o ambiente de emprego do Modelo Lagrangiano de Dispersão de Partículas, SPRAY. Particularmente a sua conexão com o modelo meteorológico de meso-escala BRAMS, sobre o qual um conjunto expressivo de pesquisadores brasileiros investiu tempo e habilidades para adequá-lo a especifidades brasileiras. Nesse sentido, também, o uso dos dados de inicialização do CPTEC preenche lacunas regionais das bases de dados de uso global.

A nós interessava particularmente o uso do SPRAY com o BRAMS. Mas essa interface estava voltada a versões mais antigas do RAMS, sendo necessário adequá-la às mudanças introduzidas nas novas versões. Tivemos que trabalhar nestes ajustes, o que foi feito em interações sucessivas com pesquisadores da Arianet. Um momento intenso desse processo ocorreu durante a preparação e realização do Workshop on Air Quality Modeling, de 23 a 27 de fevereiro de 2015, quando participantes do evento tiveram oportunidade de trabalhar com o modelo SPRAY, sob orientação do Dr. Gianni Tinarelli. No processo preparatório do evento contribuímos para a instalação e ajustes dos softwares, particularmente relativos à interface BRAMS/SPRAY. Contribuímos com a correção de alguns bugs no modelo SPRAY disponível para pesquisa, em especial um relacionado à condição de contorno para a velocidade no solo.

Foram 2 anos até ter esse sistema de modelos bem acoplados e funcionando corretamente, inclusive com ferramentas de extração de dados para validação das simulações e processamentos gráficos. Neste último aspecto em particular, trabalhamos o acoplamento com softwares livres, em sistema operacional Linux, que oferece resolução gráfica melhor que o processador usual do SPRAY.

Nosso trabalho mostra que esse sistema BRAMS/SPRAY hoje está operacional e disponível para quem desejar utilizar um bom modelo lagrangiano estocástico de dispersão de partículas (No site \url{http://fap.if.usp.br/~ate/mestrado.html} estão os arquivos de configuração para o acoplamento, bem como o resultado das simulações).

\subsection{Configuração do modelo de dispersão de poluentes}

Para a dispersão horizontal de poluentes, utilizou-se uma função gaussiana para a distribuição de probabilidades da velocidade estocástica, enquanto que na vertical, foi utilizada uma Gram-Chalier truncada na terceira ordem, posto que esta é computacionalmente mais eficiente que uma Bi-gaussiana refletida (também disponível como opção).

Foi utilizado incremento de tempo variável, com tempo máximo de 30s.
O comprimento horizontal da caixa de cálculo utilizado para calcular as concentrações foi de 1km na simulação do $MP_{10}$ em Cubatão, e  de 3km para a do CO.

\subsubsection{Simulação das fontes de CO para RMSP e sua relação com $MP_{10}$}

A simulação da dispersão de CO foi realizada utilizando-se as informações fornecidas pela grade 2 do BRAMS, com resolução de 3Km.
Como dito anteriormente, as estimativas da CETESB para o ano do experimento apontam que 98\% da emissão de CO na RMSP vinham de veículos movidos a combustíveis fósseis. Desta forma, considerou-se apenas as emissões desta fonte, desprezando-se as demais. Buscamos montar esta fonte para a simulação, com uma resolução mais refinada e dados mais atualizados do sistema viário e da densidade e composição da frota circulante na RMSP. Cremos estar próximos de concluir esta tarefa, mas não foi possível completá-la nos limites de tempo e prioridades para a conclusão deste trabalho. Mas consideramos que a solução adotada para equacionar esta fonte é bastante aceitável.

Baseamo-nos no trabalho de \citet{Kerr2005CO_FontesHarmo10}, que empregou emissões de CO avaliadas a partir da densidade de trafego veicular nas vias da RMSP, estimada por um programa da CET, e da taxa média de emissão da frota de veículos para o ano 2000. A resolução desta fonte área era de $5X5km^2$. As emissões integradas ao longo de um ano representavam um total de 1,69 milhões de toneladas, valor bastante próximo das 1,62 milhões de toneladas estimadas então pela CETESB. \citep[Relatório para o ano 2000]{cetesbReport}. Considerando-se que neste período de 2000 a 2006 as mudanças no sistema viário foram pequenas, utilizou-se essa mesma estrutura de fonte para o CO, mas aplicando-se um fator de correção devido às significativas reduções nas emissões veiculares de CO introduzidas pelo PROCONVE. A razão entre as concentrações de CO no ano 2000 e 2006 foi de 1,5, tomando-se por base medidas realizadas pela CETESB em sua estação de Congonhas, na Avenida dos Bandeirantes.  Tratava-se de uma artéria representativa do sistema viário e esse ponto tem captação significativa das emissões veiculares primárias devido à sua estreita proximidade à linha de tráfego. A razão calculada mostra-se coerente com a progressiva renovação da frota veicular com unidades dotadas de injeção eletrônica e catalisadores no sistema de exaustão. Da mesma forma esta estação foi escolhida para construir a modulação temporal da intensidade de emissão de CO ao longo do dia (figura \ref{fig:PM10CO_ModulacaoTemporal}).
\newpage
\begin{figure}[H]
  \centering
  \includegraphics[width = 0.8\linewidth]{figuras/metodos/FonteCO.png}
  \caption{Representação da fonte de CO feita com 169 quadrados de $5X5km^2$, representando a emissão veicular deste poluente na RMSP (valores registrados nas células em kg/h).}
  \label{fig:FonteCO}
\end{figure}

Não dispúnhamos de uma estimativa de emissões direta de material particulado relacionada a veículos. Essa, inclusive, não é uma tarefa fácil uma vez que, além da exaustão, há contribuição por desgaste de partes dos veículos (pneus,freios,erosão de peças etc), a ressuspensão de solo, ou o próprio aerossol secundário que pode ter proporções significativas. Supondo novamente que a estação Congonhas capta particularmente as emissões primárias do local, utilizamos, em uma primeira aproximação bastante simplificada, a razão entre $\frac{CO}{MP_{10}}=40,7$ nesta estação para definir, a partir da fonte área de CO, o que seria a contribuição direta de  $MP_{10}$ associada à fonte veicular na RMSP. A semelhança entre as modulações temporais das concentrações de CO e $PM_{10}$ na estação Congonhas pode ser vista na figura \ref{fig:PM10CO_ModulacaoTemporal}, sendo a correlação entre ambas = 0,4, com  nível de significância < 0,05. Essa simplificação nos parece razoável para as áreas altamente urbanizadas, mas é bastante limitada para áreas mais periféricas onde deparamo-nos, por exemplo, com ruas sem pavimentação ou frota mais envelhecida. 
\newpage
\begin{figure}[H]
  \centering
  \includegraphics[width = 0.6\textwidth]{figuras/metodos/TimeModulationCO_PM10.png}
  \caption{Modulação das Intensidades de $MP_{10}$ e CO por hora do dia, a partir das concentrações medidas na estação de Congonhas em 2006. Há boa correlação%
  	%calcule essa correlação e indique a sua significância.
  	%
  	 entre $MP_{10}$ e CO, o que alimentou %justifica%
  	  a aproximação feita neste trabalho para relacionar as simulações de CO com $MP_{10}$ nas comparações com o experimento de inverno relacionadas à fonte veicular.}%conferir se essa aproximação foi usada
  \label{fig:PM10CO_ModulacaoTemporal}
\end{figure}
\subsubsection{Fontes $MP_{10}$ para Cubatão}%

Na simulação da dispersão de material particulado utilizamos um inventário da CETESB sobre as fontes industriais, o que totalizava uma emissão de 3.950 toneladas de $MP$ por ano. Os resultados desta simulação foram comparados com a poluição observada nas estações de Vila Parisi, Vale do Mogi, e Cubatão Centro.
As fábricas de fertilizantes, atualizando levantamento feito por \citet{kerr2000CuFontes}. Essas são as indústrias que têm maior impacto na área de Vila Parisi e em 2006 representavam 15\% do total destas emissões.
\newpage
\begin{figure}[H]
  \centering
  \includegraphics[width = 0.8\textwidth]{figuras/metodos/Fontes23-08-2006-01h.png}
  \caption{Posição das fábricas inventariadas pela CETESB em 2006 cuja emissão de $MP$ em Cubatão foi simulada pelo SPRAY.\\
           Os pontos verdes representam estações de monitoramento da CETESB.\\
           O ponto verde extremo a NE também continha a estação Vale do Mogi do experimento de inverno.\\
           O ponto preto é a Refinaria Presidente Bernardes.\\
           Os pontos marrons são as fábricas de Fertilizantes.\\
           Os pontos azuis são fábricas de outros ramos.\\
           Todas as fábricas estão, no máximo a 10m do nível do mar. Notar as limitações na resolução da topografia no BRAMS
          }
  \label{fig:FontesMP10}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width = 0.9\textwidth]{figuras/metodos/FontesMP10marble.png}
  \caption{Posição das fábricas em Cubatão em mapa do projeto Open Street Maps.\\
           Companhia Brasileira do Estireno não aparece no mapa, mas fica próximo à Ultrafertil (Cubatão), nos entornos da SP-148.\\
           Gerado com o software Marble.\\
          }
  \label{fig:FontesMP10Mapa}
\end{figure}


\begin{table}[H]
  \centering
  \caption{Localização geográfica de fontes industriais em Cubatão.}
  \scriptsize
  \input{tabelas/metodos/FontesCubatao}
  \label{table:FontesMP10}
\end{table}

% Descrição das fontes de MP10 atualizadas. Atenágoras
 
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../../dissertacao"
%%% End: 
