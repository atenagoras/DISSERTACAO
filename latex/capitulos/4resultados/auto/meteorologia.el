(TeX-add-style-hook "meteorologia"
 (lambda ()
    (LaTeX-add-labels
     "eq:sigma_vento"
     "table:vento_estacoes"
     "table:umidade"
     "table:temperatura")
    (TeX-run-style-hooks
     "capitulos/4resultados/tabelas/ventoestacoes_nohead"
     "capitulos/4resultados/tabelas/umidade"
     "capitulos/4resultados/tabelas/temperatura")))

