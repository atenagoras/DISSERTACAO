(TeX-add-style-hook "expInvernoSPRAY"
 (lambda ()
    (LaTeX-add-labels
     "fig:TransportePM10"
     "fig:Vento15hPM10"
     "fig:Vento17hPM10"
     "fig:Vento23hPM10"
     "fig:VentoTransportePM10"
     "table:PM10")
    (TeX-run-style-hooks
     "tabelas/resultados/PM10")))

