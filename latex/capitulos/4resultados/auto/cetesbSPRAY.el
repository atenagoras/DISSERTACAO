(TeX-add-style-hook "cetesbSPRAY"
 (lambda ()
    (LaTeX-add-labels
     "table:CO"
     "fg:SaoCaetanoCO"
     "fg:COsolo"
     "fg:COcorte"
     "fg:TransporteCO")
    (TeX-run-style-hooks
     "capitulos/4resultados/tabelas/CO")))

