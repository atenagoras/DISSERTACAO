\section{Meteorologia}
Na tabela \ref{table:Estacoes}, são apresentadas as estações da CETESB e também as do experimento de inverno,
seus números de referência e os parâmetros medidos (meteorológicos e poluentes).

\begin{table}[H]
  \centering
  \caption{Estações meteorológicas e/ou de monitoramento de poluentes}
  \input{capitulos/3resultados/tabelas/estacoes}
  \label{table:Estacoes}
\end{table}

As tabelas \ref{table:vento_estacoes}, \ref{table:umidade}, e \ref{table:temperatura} mostram a comparação dos resultados 
da simulação meteorológica com as medidas nas estações, respectivamente, para os parâmetros vento, umidade e temperatura.
Na tabela \ref{table:ventos_estacoes}, os índices ``u'' e ``v'' indicam, respectivamente, as componentes horizontais do vento.
Em todas as tabelas em que há comparação de parâmetros, ``r'' é a correlação entre os dados simulados e observados, $\sigma_o$
 e $\sigma_s$ são, respectivamente, os desvios padrão dos valores observados e simulados. RMSE é o ``Root Mean Square Error''
 (Raíz do Erro Quadrático Médio). No caso da tabela \ref{table:vento_estacoes}, o RMSVE é o ``Root Mean Square Vector Error''
 (Raíz do Erro Quadrático Vetorial Médio). O desvio padrão para o vento, como se trata de uma grandeza vetorial foi calculado como:

 \begin{equation}
   \label{eq:sigma_vento}
   \sigma(u.v)=\sqrt{\sigma^{2}_u+\sigma^{2}_v}
 \end{equation}

 Quanto maior é o ``r'', melhor a correlação entre os dados simulados e observado. Um número grande de valores comparados também 
torna o resultado significativo (Com um alto nível de significância P), mesmo que o o ``r'' seja baixo.
Valores similares de RMSE, (ou RMSVE, no caso da comparação de ventos) e desvios padrões observados e simulados 
($\sigma_o$ e $\sigma_s$, respectivamente) para os diferentes parâmetros também indicam quão próxima a simulação está dos valores observados.

\begin{table}[H]
	\centering
	\caption{Comparação entre os ventos observados e simulados nas estaçÕes}
	% Mesmo que este arquivo esteja em outro diretório e seja chamado pelo arquivo-pai,
	% deve-se chamar as tabelas, figuras etc com o caminho em relação ao diretório do arquivo principal
	\input{capitulos/3resultados/tabelas/ventoestacoes_nohead} %caminho em função do arquivo principal (dissertacao.tex)
	\label{table:vento_estacoes}
\end{table}

\begin{table}[H]
  \centering
  \caption{Comparação entre as umidades observada e simulada nas estações}
  \input{capitulos/3resultados/tabelas/umidade}
  \label{table:umidade}
\end{table}

\begin{table}[H]
  \centering
  \caption{Comparação entre as temperaturas observadas e simuladas nas estações}
  \input{capitulos/3resultados/tabelas/temperatura}
  \label{table:temperatura}
\end{table}

Os resultados mostram que os parâmetros meteorológicos foram bem simulados pelo BRAMS.
O vento horizontal a 10m de altura teve um alto valor de significância, e o RMSVE e o $\sigma_s(u,v)$
tiveram valors menores ou próximos ao $\sigma_s(u,x)$. O mesmo pode ser dito sobre a umidade e a temperatura 
nas tabelas \ref{table:umidade} e \ref{table:temperatura}, com exceção para a umidade na estação 2, que está próxima ao mar e de montanhas,
cuja variação de altitude entre 600 a 1200m é muito grande para ser captada pela resolução utilizada no modelo.

