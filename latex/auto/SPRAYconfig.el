(TeX-add-style-hook "SPRAYconfig"
 (lambda ()
    (LaTeX-add-labels
     "sec:SPRAY"
     "sec:acoplamentoBRAMS-SPRAY"
     "fig:FonteCO"
     "fig:PM10CO_ModulacaoTemporal"
     "fig:FontesMP10"
     "fig:FontesMP10Mapa"
     "table:FontesMP10")
    (TeX-run-style-hooks
     "tabelas/metodos/FontesCubatao")))

