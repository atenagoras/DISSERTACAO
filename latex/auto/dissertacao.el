(TeX-add-style-hook "dissertacao"
 (lambda ()
    (LaTeX-add-bibliographies
     "bibliografia/ref")
    (TeX-run-style-hooks
     "latex2e"
     "rep12"
     "report"
     "twoside"
     "12pt"
     "a4paper"
     "inicio/packages"
     "inicio/start"
     "capitulos/1introducao/apresentacao"
     "capitulos/1introducao/estadoSP"
     "capitulos/1introducao/DescricaoPoluentes"
     "capitulos/2Teoria/TeoriaDifusao"
     "capitulos/3metodos/Intro"
     "capitulos/3metodos/BRAMSconfig"
     "capitulos/3metodos/SPRAYconfig"
     "capitulos/3metodos/Estacoes"
     "capitulos/3metodos/Estatistica"
     "capitulos/4resultados/meteorologia2"
     "capitulos/4resultados/radiossondagem"
     "capitulos/4resultados/cetesbSPRAY2"
     "capitulos/4resultados/expInvernoSPRAY"
     "capitulos/5conclusao/conclusao")))

