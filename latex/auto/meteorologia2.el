(TeX-add-style-hook "meteorologia2"
 (lambda ()
    (LaTeX-add-labels
     "sec:vento-meridional"
     "sec:magnitude")
    (TeX-run-style-hooks
     "../output/SIMvsOBS/tabelas/meteo/DesvioMediaPressao"
     "../output/SIMvsOBS/tabelas/meteo/PerformancePressao"
     "../output/SIMvsOBS/tabelas/meteo/DesvioMediaUmidade"
     "../output/SIMvsOBS/tabelas/meteo/PerformanceUmidade"
     "figuras/resultados/Umidade"
     "../output/SIMvsOBS/tabelas/meteo/DesvioMediaTemperatura"
     "../output/SIMvsOBS/tabelas/meteo/PerformanceTemperatura"
     "figuras/resultados/Temperatura"
     "../output/SIMvsOBS/tabelas/meteo/uDesvioMediaVento"
     "../output/SIMvsOBS/tabelas/meteo/uPerformanceVento"
     "figuras/resultados/VentoU"
     "../output/SIMvsOBS/tabelas/meteo/vDesvioMediaVento"
     "../output/SIMvsOBS/tabelas/meteo/vPerformanceVento"
     "figuras/resultados/VentoV"
     "../output/SIMvsOBS/tabelas/meteo/magDesvioMediaVento"
     "../output/SIMvsOBS/tabelas/meteo/magPerformanceVento"
     "../output/SIMvsOBS/tabelas/meteo/magTesteTVento"
     "figuras/resultados/RosaDosVentos")))

