(TeX-add-style-hook "Estacoes"
 (lambda ()
    (LaTeX-add-labels
     "sec:estacoes"
     "fig:MapaEstacoes"
     "table:Estacoes")
    (TeX-run-style-hooks
     "tabelas/resultados/estacoes")))

