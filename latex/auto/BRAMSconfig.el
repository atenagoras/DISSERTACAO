(TeX-add-style-hook "BRAMSconfig"
 (lambda ()
    (LaTeX-add-labels
     "sec:brams"
     "sec:BRAMSconfig")
    (TeX-run-style-hooks
     "tabelas/metodos/Grade"
     "figuras/metodos/Mapa2Grades")))

