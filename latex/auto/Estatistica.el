(TeX-add-style-hook "Estatistica"
 (lambda ()
    (LaTeX-add-labels
     "sec:Estatistica"
     "eq:correlacao"
     "eq:RMSE"
     "eq:RMSVE"
     "eq:FractionalBias"
     "eq:NMSE"
     "sec:CriteriosAvaliacao")))

