(TeX-add-style-hook "DescricaoPoluentes"
 (lambda ()
    (LaTeX-add-labels
     "sec:ControleDePoluentes"
     "sec:proconve"
     "tab:PROCONVE"
     "fig:PROCONVEleves"
     "fig:UltrapassagemCO-CCesar"
     "fig:ConcCO_CCesar"
     "fig:ST_CCesar"
     "fig:UltrapassagemCO-Congonhas"
     "fig:ConcCOCongonhas"
     "fig:STCongonhas"
     "fig:UltrapassagemO3-Ibirapuera"
     "fig:ConcO3Ibirapuera"
     "fig:STIbirapuera"
     "fig:UltrapassagemO3-Mooca"
     "fig:ConcO3Mooca"
     "fig:STMooca"
     "sec:cubataoControle"
     "fig:ConcCubataoCentro"
     "fig:ConcVParisi"
     "fig:STanualMP10Cubatao"
     "fig:stMaxCubataoCentro"
     "fig:stMaxConcVParisi"
     "fig:STmaximasMP10Cubatao")
    (TeX-run-style-hooks
     "tabelas/introducao/PM10-CONAMA-WHO"
     "tabelas/introducao/PM25-CONAMA-WHO"
     "tabelas/introducao/NO2-CONAMA-WHO"
     "tabelas/introducao/O3-CONAMA-WHO"
     "tabelas/introducao/CO-CONAMA-WHO"
     "tabelas/introducao/SO2-CONAMA-WHO")))

