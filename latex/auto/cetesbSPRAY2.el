(TeX-add-style-hook "cetesbSPRAY2"
 (lambda ()
    (LaTeX-add-labels
     "fig:rmsp2baixada-transporteCO"
     "fg:COsolo"
     "fg:COcorte"
     "fg:TransporteCO")
    (TeX-run-style-hooks
     "../output/SIMvsOBS/tabelas/DesvioMediaCO"
     "../output/SIMvsOBS/tabelas/PerformanceCO"
     "figuras/resultados/stCO"
     "../output/SIMvsOBS/tabelas/DesvioMediaMP10"
     "../output/SIMvsOBS/tabelas/PerformanceMP10"
     "figuras/resultados/stMP10")))

