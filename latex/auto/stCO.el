(TeX-add-style-hook "stCO"
 (lambda ()
    (LaTeX-add-labels
     "fig:stCO1"
     "fig:stCO2"
     "fig:stCO3"
     "fig:stCO4"
     "fig:stCO5"
     "fig:stCO6"
     "fig:stCO7")))

